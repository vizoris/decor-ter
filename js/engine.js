
$(function() {

// Фиксированная шапка припрокрутке


// $(window).scroll(function(){
//       var sticky = $('.header'),
//           scroll = $(window).scrollTop();
//       if (scroll > 100) {
//           sticky.addClass('header-fixed');
//       } else {
//           sticky.removeClass('header-fixed');
//       };
//   });





// FansyBox
 $('.fancybox').fancybox({});





var projectSlider = $('.project-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
      }
    },
    ]
});
$('.project-slider__prev').click(function(){
    $(projectSlider).slick("slickPrev")
});
$('.project-slider__next').click(function(){
    $(projectSlider).slick("slickNext")
});




$('.product-slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 479,
      settings: {
        slidesToShow: 1,
      }
    },
    ]
});



$('.videogallery-wrap').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
      }
    },
    {
      breakpoint: 479,
      settings: {
        slidesToShow: 1,
      }
    },
    ]
});




var productSlider = $('.product-card__slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  dots: true,
  asNavFor: '.product-card__slider--nav'
});


$('.product-card__slider--nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.product-card__slider',
  dots: false,
  arrows: false,
  focusOnSelect: true,
  responsive: [
  {
    breakpoint: 768,
    settings: {
      slidesToShow: 2,
    }
  },
  {
    breakpoint: 479,
    settings: {
      slidesToShow: 1,
    }
  },
  ]
});




 // Стилизация селектов
$('select').styler();



$('.product-type__slider').slick({
  dots: false,
  arrows: true,
  speed: 300,
  slidesToShow: 7,
  slidesToScroll: 1,
  // autoplay: true,
  // autoplaySpeed: 5000,
  responsive: [
  {
    breakpoint: 550,
    settings: {
      slidesToShow: 5,
    }
  },
  {
    breakpoint: 479,
    settings: {
      slidesToShow: 4,
    }
  },
  {
    breakpoint: 380,
    settings: {
      slidesToShow: 3,
    }
  },
  ]
});


//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs-wrap').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});




// Begin of slider-range
$( "#slider-range" ).slider({
  range: true,
  min: 100,
  max: 10000,
  values: [ 2000, 5600 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( ui.values[ 0 ] );
    $( "#amount-1" ).val( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();
        if(parseInt(value1) > parseInt(value2)){
          value1 = value2;
          $("input#amount").val(value1);
        }
        $("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();

        if(parseInt(value1) > parseInt(value2)){
          value2 = value1;
          $("input#amount-1").val(value2);
        }
        $("#slider-range").slider("values",1,value2);
      });
// END of slider-range




// Меню в сайдбаре

$('.drop > a').click(function() {
  $(this).toggleClass('active');
  $(this).next('ul').fadeToggle();
});


// сайдбар на мобильном
$('.sidebar-filter__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})

})